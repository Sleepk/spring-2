package br.com.casadocodigo.loja.dao;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.casadocodigo.loja.builders.ProdutoBuilder;
import br.com.casadocodigo.loja.conf.DataSourceConfigurationTest;
import br.com.casadocodigo.loja.conf.JPAConfiguration;
import br.com.casadocodigo.loja.models.Produto;
import br.com.casadocodigo.loja.models.TipoPreco;
/**
 * Classe responsavel por criar uma rotina de testes. <br>
 * 
 * 
 * Anotação RunWith, informar que o Spring Utilizara o Junit para executar.
 * @ContextConfiguration - Estara usando as classes JPAConfigurations para carregar as configuracoes do jpa e coletar os dados do DAO.
 * @ActiveProfile - Profile ativo para teste.
 * @author Julio
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { JPAConfiguration.class, ProdutoDAO.class, DataSourceConfigurationTest.class })
@ActiveProfiles("test")
public class ProdutoDAOTest {

	@Autowired
	ProdutoDAO produtoDAO;

	@Transactional
	@Test
	public void deveSomarPrecosPorTipoLivro() {

		List<Produto> livrosImpresso = ProdutoBuilder.newProduto(TipoPreco.IMPRESSO, BigDecimal.TEN).more(3).buildAll();

		List<Produto> livrosEbook = ProdutoBuilder.newProduto(TipoPreco.EBOOK, BigDecimal.TEN).more(3).buildAll();

		livrosImpresso.stream().forEach(produtoDAO::gravar);
		livrosEbook.stream().forEach(produtoDAO::gravar);

		BigDecimal valor = produtoDAO.somaPrecosPorTipo(TipoPreco.EBOOK);

		Assert.assertEquals(new BigDecimal(40).setScale(2), valor);

	}
}
