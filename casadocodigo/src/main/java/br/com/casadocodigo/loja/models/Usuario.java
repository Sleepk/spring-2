package br.com.casadocodigo.loja.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
public class Usuario implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6515237668914804962L;
	@Id
	private String email;
	private String senha;
	private String nome;

	@OneToMany(fetch = FetchType.EAGER)
	private List<Role> roles = new ArrayList<Role>();

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.roles;
	}

	// solitando a senha
	@Override
	public String getPassword() {
		return this.senha;
	}

	// solitando o usuario
	@Override
	public String getUsername() {
		return this.nome;
	}

	// conta nao expirada
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	// conta nao bloqueada
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	// credencial nao esta expirada?
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	// esta habilitado
	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getEmail() {
		return email;
	}

	public String getSenha() {
		return senha;
	}

	public String getNome() {
		return nome;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

}