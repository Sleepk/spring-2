package br.com.casadocodigo.loja.conf;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * classe que identifica os servlets do projeto
 * 
 * @author souza
 *
 */
public class ServletSpringMVC extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { SecurityConfiguration.class, AppWebConfiguration.class, JPAConfiguration.class,
				JPAProductionConfiguration.class };
	}

	// Configuracao para reconhecer a classe de configuracao que informa
	// qual eh o controller.
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {};
	}

	// Serve para mapear o servlet do raiz do spring
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	/**
	 * metodo para deixar as letras em utf8
	 * 
	 * outra funcao para fazer o carregamento via lazy load
	 */
	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setEncoding("UTF-8");
		return new Filter[] { encodingFilter, new OpenEntityManagerInViewFilter() };
	}

	/**
	 * Metodo para aceitar trabalhar com arquivos.
	 */
	@Override
	protected void customizeRegistration(Dynamic registration) {
		registration.setMultipartConfig(new MultipartConfigElement(""));
	}

	/**
	 * Funcao para informar ao startar o projeto o qual perfil o sistema devera
	 * carregar
	 */
	// @Override
	// public void onStartup(ServletContext servletContext) throws ServletException
	// {
	// super.onStartup(servletContext);
	// servletContext.addListener(RequestContextListener.class);
	// servletContext.setInitParameter("spring.profiles.active", "dev");
	// }

}
