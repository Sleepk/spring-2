package br.com.casadocodigo.loja.conf;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.datetime.DateFormatterRegistrar;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.google.common.cache.CacheBuilder;

import br.com.casadocodigo.loja.controllers.HomeController;
import br.com.casadocodigo.loja.dao.ProdutoDAO;
import br.com.casadocodigo.loja.infra.FileSaver;
import br.com.casadocodigo.loja.models.CarrinhoCompras;

/**
 * Classe responsavel por gerenciar a configuracao do spring.
 * 
 * @author souza
 *
 */
@EnableWebMvc
@EnableCaching
@ComponentScan(basePackageClasses = { HomeController.class, ProdutoDAO.class, FileSaver.class, CarrinhoCompras.class }) // busca
																														// o
																														// arquivo
																														// home
public class AppWebConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");// para que o projeto veja os prefixos
		resolver.setSuffix(".jsp");// paginas que serao acessadas
		resolver.setExposedContextBeanNames("carrinhoCompras"); // metodo que expoe um item para o bean ficar exposto
		return resolver;
	}

	/**
	 * Metodo que carregara nossas mensagens de retorno.
	 * 
	 * @return messageSource
	 */
	@Bean
	public MessageSource messageSource() {

		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		// local onde sera encontrado o caminho do arquivo messages.properties
		messageSource.setBasename("/WEB-INF/messages");
		// tipo de enconding.
		messageSource.setDefaultEncoding("UTF-8");
		// tempo que podemos salvar o arquivo sem necessariamente ter que subir
		// novamente o servidor.
		messageSource.setCacheSeconds(1);
		return messageSource;

	}

	/**
	 * Conversor de datas do Spring
	 * 
	 * @return
	 */
	@Bean
	public FormattingConversionService mvcConversionService() {
		DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();
		DateFormatterRegistrar formatterRegistrar = new DateFormatterRegistrar();
		formatterRegistrar.setFormatter(new DateFormatter("dd/MM/yyyy"));
		formatterRegistrar.registerFormatters(conversionService);

		return conversionService;
	}

	/**
	 * Metodo que mostra para o spring trabalhar com arquivos.
	 * 
	 * @return
	 */
	@Bean
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	// template que buscara as informacoes em rest
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	// gerente de cache
	/**
	 * Maximum Size, quantos itens esta sendo usado no cache, tempo para expirar
	 * sera em 5 minutos
	 * 
	 * @return
	 */
	@Bean
	public CacheManager cacheManager() {
		CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder().maximumSize(100).expireAfterAccess(5,
				TimeUnit.MINUTES);

		GuavaCacheManager manager = new GuavaCacheManager();
		manager.setCacheBuilder(builder);

		return new ConcurrentMapCacheManager();
	}

	/**
	 * Metodo que ira identificar se a requisicao vai ser em jsp ou rest.
	 * 
	 * @param manager
	 * @return
	 */
	@Bean
	public ViewResolver contetNegotiationViewResolver(ContentNegotiationManager manager) {
		List<ViewResolver> viewResolvers = new ArrayList<ViewResolver>();
		viewResolvers.add(internalResourceViewResolver());
		viewResolvers.add(new JsonViewResolver());
		ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
		resolver.setViewResolvers(viewResolvers);
		// metodo que ira verificar o que sera retornado
		resolver.setContentNegotiationManager(manager);

		return resolver;
	}

	// esta requisao deve ser default
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	// codigo interceptador para internacionalização
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// funcao para reconhecer interceptação.
		registry.addInterceptor(new LocaleChangeInterceptor());
	}

	/**
	 * Funcao para reconhecer diretamente o locale do navegador
	 */

	@Bean
	public LocaleResolver localeResolver() {
		return new CookieLocaleResolver();
	}

	/**
	 * Funcao para envio de emails
	 * 
	 * @return
	 */
	@Bean
	public MailSender mailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		mailSender.setHost("smtp.gmail.com");
		mailSender.setUsername("alura.springmvc@gmail.com");
		mailSender.setPassword("alura2015");
		mailSender.setPort(587);

		Properties mailProperties = new Properties();
		mailProperties.put("mail.smtp.auth", true);
		mailProperties.put("mail.smpt.starttls.enable", true);

		mailSender.setJavaMailProperties(mailProperties);
		return mailSender;
	}

}
