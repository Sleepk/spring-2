package br.com.casadocodigo.loja.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.casadocodigo.loja.dao.ProdutoDAO;
import br.com.casadocodigo.loja.models.Produto;

@Controller
public class HomeController {

	@Autowired
	private ProdutoDAO produtoDAO;

	/**
	 * Cacheable, � habilitado o item para fazer cache, aonde o valor � produtoHome
	 * @return
	 */
	@RequestMapping("/") // mapear para acessar o index /
	@Cacheable(value="produtoHome")
	public ModelAndView index() {

		List<Produto> produtos = produtoDAO.listar();

		// fara o direcionamento da pagina
		ModelAndView modelAndView = new ModelAndView("home");

		return modelAndView.addObject("produtos", produtos);
	}

}
