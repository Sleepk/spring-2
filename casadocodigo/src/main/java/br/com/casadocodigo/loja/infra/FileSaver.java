package br.com.casadocodigo.loja.infra;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Classe que � responsavel por chamar a pasta responsavel por gerenciar os itens
 * @author souza
 *
 */
@Component
public class FileSaver {

	@Autowired
	private HttpServletRequest request;

	public String write(String baseFolder, MultipartFile file) {
		try {
			// pega as informacoes do path real aonde o arquivo esta
			String realPath = request.getServletContext().getRealPath("/" + baseFolder);
			// pega o path real e coloca com o nome do arquivo original
			String path = realPath + "/" + file.getOriginalFilename();
			// faz a transferencia dos dados
			file.transferTo(new File(path));;
			return baseFolder + "/" + file.getOriginalFilename();
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
