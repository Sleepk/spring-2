package br.com.casadocodigo.loja.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CarrinhoCompras implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2106272778536854933L;

	private Map<CarrinhoItem, Integer> itens = new LinkedHashMap<CarrinhoItem, Integer>();

	/**
	 * funcao que adiciona itens para o carrinho, nele é acessado o metodo
	 * getquantidade que verifica quantos itens tem dentro da chave e adiciona mais
	 * um
	 * 
	 * @param item
	 */
	public void add(CarrinhoItem item) {
		itens.put(item, getQuantidade(item) + 1);
	}

	/**
	 * metodo para informar quantos itens tem no carrinho,
	 * 
	 * funcao pega uma chave e verifica se tem item dentro do carrinho, caso nao
	 * tenha é criado um.
	 * 
	 * @param item
	 * @return
	 */
	public Integer getQuantidade(CarrinhoItem item) {
		if (!itens.containsKey(item)) {
			itens.put(item, 0);
		}

		return itens.get(item);
	}

	/**
	 * Metodo para fazer uma conta e informar a quantidade de item dentro do
	 * carrinho
	 * 
	 * @return
	 */
	public int getQuantidade() {
		return itens.values().stream().reduce(0, (proximo, acumulador) -> (proximo + acumulador));
	}

	// metodo que retornara itens do carrinho itens
	public Collection<CarrinhoItem> getItens() {
		return itens.keySet();
	}

	// coleta o total de itens
	public BigDecimal getTotal(CarrinhoItem item) {
		return item.getTotal(getQuantidade(item));
	}

	// calculo para verificar o valor total que sera cobrado do cliente.
	public BigDecimal getTotal() {
		BigDecimal total = BigDecimal.ZERO;
		for (CarrinhoItem item : itens.keySet()) {
			total = total.add(getTotal(item));
		}
		return total;
	}

	public void remover(Integer produtoId, TipoPreco tipoPreco) {
		Produto produto = new Produto();
		produto.setId(produtoId);
		itens.remove(new CarrinhoItem(produto, tipoPreco));
	}

}
