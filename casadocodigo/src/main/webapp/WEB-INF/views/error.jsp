<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>
<tags:pageTemplate titulo="Produto Não Encontrado">
	<div class="container">
		<div class="row">
			<section id="index-section" class="container middle">
				<div class="alert alert-danger" role="alert">Ocorreu um Erro
					Generico!! :(</div>
			</section>

			<!-- 
			Mensagem: ${exception.message}
			<c:forEach items="${exception.stackTrace}" var="stk">
			${stk}
			</c:forEach>
			
			 -->
		</div>
	</div>
</tags:pageTemplate>